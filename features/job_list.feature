Feature: Job List
  In order to know the offered jobs
  As a candidate
  I want to list all the offers

  Scenario: List the job with required experience not specified
    Given only a "Web Programmer" offer without required experience exists in the offers list
    When I access the offers list page
    Then I should see the job "Web Programmer" with required experience "Not Specified"

  Scenario: List the job with required experience specified
    Given only a "Python developer" offer with required experience "3" exists in the offers list
    When I access the offers list page
    Then I should see the job "Python developer" with required experience "3"