Given('only a {string} offer without required experience exists in the offers list') do |title|
  @job_offer = JobOffer.new
  @job_offer.owner = UserRepository.new.first
  @job_offer.title = title
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Given('only a {string} offer with required experience {string} exists in the offers list') do |title, required_experience|
  @job_offer = JobOffer.new
  @job_offer.owner = UserRepository.new.first
  @job_offer.title = title
  @job_offer.required_experience = required_experience
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.is_active = true

  JobOfferRepository.new.save @job_offer
end

Then('I should see the job {string} with required experience {string}') do |title, required_experience|
  page.should have_content(title)
  find('td.required_experience', text: required_experience)
end
