When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

When('I fill the title with {string}') do |offer_title|
  fill_in('job_offer[title]', with: offer_title)
end

When('I fill the title with {string} and required experience with {int}') do |offer_title, required_experience|
  fill_in('job_offer[title]', with: offer_title)
  fill_in('job_offer[required_experience]', with: required_experience)
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then('I should see {string} in My Offers') do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then('I should not see {string} in My Offers') do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Then('I should see {string} with required experience {string} in My Offers') do |title, required_experience|
  visit '/job_offers/my'
  page.should have_content(title)
  find('td.required_experience', text: required_experience)
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete')
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', with: new_title)
end

Given(/^I save the modification$/) do
  click_button('Save')
end
