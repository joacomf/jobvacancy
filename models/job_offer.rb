class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title, :required_experience,
                :location, :description, :is_active,
                :updated_on, :created_on

  validates :title, presence: true
  validates :required_experience, numericality: { only_integer: true,
                                                  greater_than_or_equal_to: 0 }

  def initialize(data = {})
    @id = data[:id]
    @title = data[:title]
    @required_experience = data[:required_experience].to_i
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end

  def required_experience_render
    return 'Not Specified' if @required_experience.zero?

    @required_experience
  end
end
