Sequel.migration do
  up do
    add_column :job_offers, :required_experience, Integer, default: 0
  end

  down do
    drop_column :job_offers, :required_experience
  end
end
